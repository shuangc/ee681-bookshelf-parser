#include "placement.h"
#include <stdlib.h>
#include <string.h>

Plc_Pin_t * Plc_PinAlloc()
{
	Plc_Pin_t * pPin;
	pPin = (Plc_Pin_t *)malloc(sizeof(Plc_Pin_t));
	return pPin;
}

void Plc_PinFree( Plc_Pin_t * pPin)
{
    Plc_VecRemove(pPin->pNode->vPins, pPin);
    Plc_VecRemove(pPin->pNet->vPins, pPin);
	free(pPin);
}
