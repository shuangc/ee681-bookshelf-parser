#include "placement.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// void bookshelfParser::insertPin2Net( Net *net, Cell *cell, char *io_type, double px, double py)
static void Plc_AddPinToNet( Plc_Net_t * pNet, Plc_Node_t * pNode, char * szDir, double dX, double dY )
{
    // px *= unitScale;
    // py *= unitScale;

    Plc_Pin_t * pPin = Plc_PinAlloc();
    if (szDir[0] == 'O')
        pPin->eIO = PLC_PIN_DIR_OUT;
    else if (szDir[0] == 'I')
        pPin->eIO = PLC_PIN_DIR_IN;
    else if (szDir[0] == 'B')
        pPin->eIO = PLC_PIN_DIR_BI;
    else
    {
        fprintf(stderr, "Error: Invalid pin direction.\n");
        exit(-1);
    }
    pPin->dX       = dX;
    pPin->dY       = dY;
    pPin->pNet  = pNet;
    pPin->pNode = pNode;
    Plc_VecPush(pNode->vPins, pPin);
    Plc_VecPush(pNet->vPins, pPin);
}


// void bookshelfParser::readFromNetsFile(const char *filename)
static void Plc_ManReadFromNetsFile( Plc_Man_t * pMan, const char *szFilename )
{
    FILE * flIn;
	char *szLine = NULL;
	char *pPos;
	size_t nBuf = 0;
	int nNets, nPins;
	int iNet;
	int nPad = 0;
	
    if ((flIn = fopen(szFilename, "r")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open input file: %s\n", szFilename);
		exit(-1);
	}

	printf("Reading from file: %s\n", szFilename);

    /*read title data*/
	getline(&szLine, &nBuf, flIn);
    do
    {
		getline(&szLine, &nBuf, flIn);
    }
	while (szLine[0]=='#');

	if ((pPos = strchr(szLine, ':')) != NULL)
	{
		sscanf(pPos + 1, "%d", &nNets);
	}
	else
	{
		getdelim(&szLine, &nBuf, ':', flIn);
		fscanf(flIn, "%d", &nNets);
	}
    getdelim(&szLine, &nBuf, ':', flIn);
	fscanf(flIn, "%d", &nPins);

	iNet = -1;
	while (getdelim(&szLine, &nBuf, ':', flIn) > 0)
    {
        int nPinsInNet = 0;
		char szName[40];
		char szTmp[37];
		int i;
        Plc_Net_t * pNet;
        if (fscanf(flIn, "%d", &nPinsInNet)<1)
        {
            continue;
        }
        getline(&szLine, &nBuf, flIn);
		strcpy(szName, "net");
		snprintf(szTmp, 36, "%d", ++iNet);	
        szTmp[36] = '\0';
		strcat(szName, szTmp);
        pNet = Plc_NetAlloc(szName);
		pNet->pMan = pMan;
        pNet->uId = iNet;
        pNet->nDeg = 0;
		Plc_VecPush(pMan->vNets, pNet);

        for (i = 0; i < nPinsInNet; ++i)
        {
            double dX = 0, dY = 0;
			char szName[80];
			char szIO[10];
			Plc_Node_t * pNode;

			fscanf(flIn, "%s %s", szName, szIO);
            getdelim(&szLine, &nBuf, ':', flIn);
			fscanf(flIn, "%lf %lf", &dX, &dY);
			if ((pNode = Plc_ManFindNodeName(pMan, szName)) != NULL)
			{
				Plc_AddPinToNet(pNet, pNode, szIO, dX, dY);
				++(pNet->nDeg);
				if (pNode->eType == PLC_NODE_TYPE_PAD)
				{
					++nPad;
				}
			}
        }
    }
    fclose(flIn);
	free(szLine);
	printf("%d pad(s) connected to net.\n", nPad);
}


// void bookshelfParser::readFromNodesFile(const char *filename)
static void Plc_ManReadFromNodesFile( Plc_Man_t * pMan, const char * szFilename )
{
	FILE * flIn;
	char *szLine = NULL;
    char *pPos;
	size_t nBuf = 0;
	int nNodes, nPads;
	unsigned int uId = 0;
	
    if ((flIn = fopen(szFilename, "r")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open input file: %s\n", szFilename);
		exit(-1);
	}

	printf("Reading from file: %s\n", szFilename);

    /*read preambles*/
	getline(&szLine, &nBuf, flIn);
    do
    {
		getline(&szLine, &nBuf, flIn);
    }
	while (szLine[0]=='#');

	if ((pPos = strchr(szLine, ':')) != NULL)
	{
		sscanf(pPos + 1, "%d", &nNodes);
	}
	else
	{
		getdelim(&szLine, &nBuf, ':', flIn);
		fscanf(flIn, "%d", &nNodes);
	}
    getdelim(&szLine, &nBuf, ':', flIn);
	fscanf(flIn, "%d", &nPads);
	getline(&szLine, &nBuf, flIn);

    while (getline(&szLine, &nBuf, flIn) > 0)
    {
		char szName[80], szTmp[80];
		Plc_Node_t * pNode;
		double dW, dH;
		if (sscanf(szLine, "%s %lf %lf %s", szName, &dW, &dH, szTmp) < 3)
		{
			continue;
		}
		
        pNode = Plc_NodeAlloc(szName);
		pNode->pMan = pMan;
		pNode->uId = uId++;
		pNode->dW = dW;
		pNode->dH = dH;
        if (strcmp(szTmp, "terminal") == 0)           //a terminal
        {
			pNode->eType = PLC_NODE_TYPE_PAD;
        }
        else
        {
			pNode->eType = PLC_NODE_TYPE_CELL;
        }
		Plc_VecPush(pMan->vNodes, pNode);
        Plc_HashInsert(pMan->hNodes, pNode->szName, pNode);
    }
    fclose(flIn);
	free(szLine);
}


// void bookshelfParser::readFromPlFile(const char *filename)
static void Plc_ManReadFromPlFile( Plc_Man_t * pMan, const char * szFilename )
{
	FILE * flIn;
	char *szLine = NULL;
	size_t nBuf = 0;
    char szName[80], szOri[80], szTmp[80];
	Plc_Node_t * pNode;
    double dX, dY;
	
    if ((flIn = fopen(szFilename, "r")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open input file: %s\n", szFilename);
		exit(-1);
	}

	printf("Reading from file: %s\n", szFilename);

	getline(&szLine, &nBuf, flIn);
    while (getline(&szLine, &nBuf, flIn) > 0)
    {
		if (szLine[0] == '#')
		{
			continue;
		}

		if (sscanf(szLine, "%s %lf %lf %s %s", szName, &dX, &dY, szTmp, szOri) < 5)
		{
			continue;
		}
		
		if ((pNode = Plc_ManFindNodeName(pMan, szName)) != NULL)
		{
			pNode->dX = dX;
			pNode->dY = dY;
			
			// TODO: Check if this is correct
			if (strcmp(szOri, "N") == 0)
				pNode->eOri = PLC_NODE_ORI_B0;
			else if (strcmp(szOri, "E") == 0)
				pNode->eOri = PLC_NODE_ORI_B90;
			else if (strcmp(szOri, "S") == 0)
				pNode->eOri = PLC_NODE_ORI_B180;
			else if (strcmp(szOri, "W") == 0)
				pNode->eOri = PLC_NODE_ORI_B270;
			else if (strcmp(szOri, "FN") == 0)
				pNode->eOri = PLC_NODE_ORI_F0;
			else if (strcmp(szOri, "FE") == 0)
				pNode->eOri = PLC_NODE_ORI_F90;
			else if (strcmp(szOri, "FS") == 0)
				pNode->eOri = PLC_NODE_ORI_F180;
			else if (strcmp(szOri, "FW") == 0)
				pNode->eOri = PLC_NODE_ORI_F270;
		}
    }
	
	fclose(flIn);
	free(szLine);
}

// void sclParser::readFromSclFile(const char *filename)
static void Plc_ManReadFromSclFile(Plc_Man_t * pMan, const char * szFilename)
{
	FILE * flIn;
	char * szLine = NULL;
	char * pPos;
	int nRows;
	int iRow;
	size_t nBuf = 0;
	Plc_Row_t * pRow;
	
    if ((flIn = fopen(szFilename, "r")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open input file: %s\n", szFilename);
		exit(-1);
	}

	printf("Reading from file: %s\n", szFilename);

    /*read title data*/
	getline(&szLine, &nBuf, flIn);
    do
    {
		getline(&szLine, &nBuf, flIn);
    }
	while (szLine[0]=='#');

	if ((pPos = strchr(szLine, ':')) != NULL)
	{
		sscanf(pPos + 1, "%d", &nRows);
	}
	else
	{
		getdelim(&szLine, &nBuf, ':', flIn);
		fscanf(flIn, "%d", &nRows);
	}

    for (iRow = 0; iRow < nRows; ++iRow)
    {
		int nSites;
		double dX, dY, dW, dH;
		
		while (getline(&szLine, &nBuf, flIn) > 0)
        {
            if(strncmp(szLine, "End", 3) == 0)
			{
                break;
			}
			
            if(strstr(szLine, "Coordinate") != NULL)
			{
				if ((pPos = strrchr(szLine, ':')) != NULL)
				{
					sscanf(pPos + 1, "%lf", &dY);
				}
			}
			else if (strstr(szLine, "Height") != NULL)
			{
				if ((pPos = strrchr(szLine, ':')) != NULL)
				{
					sscanf(pPos + 1, "%lf", &dH);
				}
			}
			else if (strstr(szLine, "Sitewidth") != NULL)
			{
				if ((pPos = strrchr(szLine, ':')) != NULL)
				{
					sscanf(pPos + 1, "%lf", &dW);
				}
			}
			else if (strstr(szLine, "SubrowOrigin") != NULL)
			// TODO: Support multiple subrows
			{
				if ((pPos = strchr(szLine, ':')) != NULL)
				{
					sscanf(++pPos, "%lf", &dX);
				}
				if ((pPos = strchr(pPos, ':')) != NULL)
				{
					sscanf(++pPos, "%d", &nSites);					
				}
			}
        }
		
		pRow = Plc_RowAlloc();
        pRow->pMan = pMan;
        pRow->eOri = PLC_ROW_ORI_H;
		pRow->dX = dX;
		pRow->dY = dY;
		pRow->dH = dH;
		pRow->dW = dW*nSites;
		Plc_VecPush(pMan->vRows, pRow);
    }

    fclose(flIn);
	free(szLine);
}


// Model  * bookshelfParser::read(const char *auxFileName)
Plc_Man_t * Plc_PlcFromBookshelf( const char *szAuxFilename )
{
	FILE * flIn;
	char *szLine = NULL;
	char szName[80];
	char szNodeFilename[80] = "";
	char szNetFilename[80] = "";
	char szPlFilename[80] = "";
	char szSclFilename[80] = "";
	size_t nBuf = 0;
	Plc_Man_t * pMan;
	char * pPos;
	
    if ((flIn = fopen(szAuxFilename, "r")) == NULL)
	{
		fprintf(stderr, "Error: Cannot open input file: %s\n", szAuxFilename);
		exit(-1);
	}

	printf("Reading from file: %s\n", szAuxFilename);

	strncpy(szName, szAuxFilename, 79);
	szName[79] = '\0';
	if ((pPos = strrchr(szName, '.')) != NULL)
	{
		*pPos = '\0';
	}
	pMan = Plc_ManAlloc(szName);
	
	getdelim(&szLine, &nBuf, ':', flIn);
	while (fscanf(flIn, "%s", szName) > 0)
	{
		if ((pPos = strrchr(szName, '.')) == NULL)
		{
			continue;
		}

		if (strcmp(pPos+1, "nodes") == 0)
		{
			strncpy(szNodeFilename, szName, 79);
			szNodeFilename[79] = '\0';
		}
		else if (strcmp(pPos+1, "nets") == 0)
		{
			strncpy(szNetFilename, szName, 79);
			szNodeFilename[79] = '\0';
		}
		else if (strcmp(pPos+1, "pl") == 0)
		{
			strncpy(szPlFilename, szName, 79);
			szPlFilename[79] = '\0';			
		}
		else if (strcmp(pPos+1, "scl") == 0)
		{
			strncpy(szSclFilename, szName, 79);
			szSclFilename[79] = '\0';			
		}
	}
	
	if (strlen(szNodeFilename) == 0 ||
		strlen(szNetFilename) == 0 ||
		strlen(szPlFilename) == 0 ||
        strlen(szSclFilename) == 0)
	{
		fprintf(stderr, "Error: Incomplete specification.\n");
		exit(-1);
	}
	Plc_ManReadFromNodesFile(pMan, szNodeFilename);
	Plc_ManReadFromNetsFile(pMan, szNetFilename);
	Plc_ManReadFromPlFile(pMan, szPlFilename);
    Plc_ManReadFromSclFile(pMan, szSclFilename);

    return pMan;
}


/*
void bookshelfParser::write( Model * chip, const char *filename)
{
    model = chip;
    write2PlFile(filename);
}


void bookshelfParser::write2PlFile( const char *filename)
{
    char   outFilename[80];
    strcpy(outFilename, filename);
    strcat(outFilename, ".pl");
    ofstream fout;
    fout.open(outFilename);

    if (!fout)
    {
        cerr << "(E) Error opening output file: " << outFilename
            << endl;
        exit(-1);
    }

    fout << "UCLA pl 1.0\n";
    fout << "# Created\n";
    fout << "# Platform\n\n";

    for (int i = 0; i < model->cellList.size(); i++ )
    {
        Cell * node = model->cellList[i];
        fout << " " << node->name << " "
            << node->x << " "
            << node->y << " : ";
        if ( node->ori == B0 )
            fout << "N";
        else if ( node->ori == B90 )
            fout << "E";
        else if ( node->ori == B180)
            fout << "S";
        else if ( node->ori == B270 )
            fout << "W";
        else if (node->ori == F180)
            fout << "FN";
        else if (node->ori == F90)
            fout << "FE";
        else if (node->ori == F0)
            fout << "FS";
        else if (node->ori == F270)
            fout << "FW";
        else
        {
            cerr << "(E) : Invalid orientation !" << endl;
            exit(0);
        }
        fout << endl;
    }
    fout.close();
}
*/
