#include "placement.h"
#include <stdlib.h>
#include <string.h>

Plc_Node_t * Plc_NodeAlloc( const char * szName )
{
	Plc_Node_t * pNode;
	pNode = (Plc_Node_t *)malloc(sizeof(Plc_Node_t));
	strncpy(pNode->szName, szName, 79);
	pNode->szName[79] = '\0';
	pNode->vPins = Plc_VecAlloc(0);
	return pNode;
}

void Plc_NodeFree( Plc_Node_t * pNode)
{
    while (Plc_VecSize(pNode->vPins) > 0)
    {
        Plc_PinFree(Plc_VecTypeHead(pNode->vPins, Plc_Pin_t));
    }
	free(pNode->vPins);
	free(pNode);
}
