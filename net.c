#include "placement.h"
#include <stdlib.h>
#include <string.h>

Plc_Net_t * Plc_NetAlloc( const char * szName )
{
	Plc_Net_t * pNet;
	pNet = (Plc_Net_t *)malloc(sizeof(Plc_Net_t));
	strncpy(pNet->szName, szName, 39);
	pNet->szName[39] = '\0';
	pNet->vPins = Plc_VecAlloc(0);
	return pNet;
}

void Plc_NetFree( Plc_Net_t * pNet)
{
    while (Plc_VecSize(pNet->vPins) > 0)
    {
        Plc_PinFree(Plc_VecTypeHead(pNet->vPins, Plc_Pin_t));
    }
	free(pNet->vPins);
	free(pNet);
}
