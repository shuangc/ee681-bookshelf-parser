#include "placement.h"
#include <stdio.h>

int main()
{
    Plc_Man_t * pMan;
    Plc_Node_t * pNode;
    Plc_Net_t * pNet;
    Plc_Pin_t * pPin;
    unsigned int i, j;
    pMan = Plc_PlcFromBookshelf("adaptec5.aux");
    // pMan = Plc_PlcFromBookshelf("Peko01.aux");
    Plc_VecIterate(pMan->vNodes, Plc_Node_t, pNode, i)
    {
        printf("%s\t%lf\t%lf\n", pNode->szName, pNode->dW, pNode->dH);
    }
    Plc_VecIterate(pMan->vNets, Plc_Net_t, pNet, i)
    {
        printf("%s\t%u\n", pNet->szName, pNet->nDeg);
        Plc_VecIterate(pNet->vPins, Plc_Pin_t, pPin, j)
        {
            printf("\t%s\t%lf\t%lf\n", pPin->pNode->szName, pPin->dX, pPin->dY);
        }
    }
    Plc_ManFree(pMan);

    return 0;
}
