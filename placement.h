#ifndef PLACEMENT_H__
#define PLACEMENT_H__

////////////////////////////////////////////////////////////////////////
///                          INCLUDES                                ///
////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

////////////////////////////////////////////////////////////////////////
///                    STRUCTURE DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////

typedef		struct Plc_Manager		Plc_Man_t;
typedef		struct Plc_Node			Plc_Node_t;
typedef		struct Plc_Net			Plc_Net_t;
typedef		struct Plc_Row			Plc_Row_t;
typedef		struct Plc_Pin			Plc_Pin_t;
typedef		struct Plc_Vec			Plc_Vec_t;
typedef     struct Plc_Hash         Plc_Hash_t;
typedef     enum Plc_Pin_Dir        Plc_Pin_Dir_t;
typedef     enum Plc_Node_Type      Plc_Node_Type_t;
typedef     enum Plc_Node_Ori       Plc_Node_Ori_t;
typedef     enum Plc_Row_Ori        Plc_Row_Ori_t;

// direction of pin, may not be used 
// PLC_PIN_DIR_IN :    input pin
// PLC_PIN_DIR_OUT:    output pin
// PLC_PIN_DIR_BI :    bi-directional pin

enum Plc_Pin_Dir
{
    PLC_PIN_DIR_IN = 1, PLC_PIN_DIR_OUT = 2, PLC_PIN_DIR_BI = 3
};

enum Plc_Node_Type
{
	PLC_NODE_TYPE_CELL = 0, PLC_NODE_TYPE_PAD = 1
};
// orientation w.r.t the center of the instance:
// PLC_ORI_BO:            - normal
// PLC_ORI_B90:           - clockwise rotate 90' from normal
// PLC_ORI_B180 :         - clockwise rotate 180' from normal
// PLC_ORI_B270:          - clockwise rotate 270' from normal
// PLC_ORI_F0:            - flip back along x-direction
// PLC_ORI_F90            - clockwise rotate 90' from flip-back
// PLC_ORI_F180           - clockwise rotate 180' from flip-back
// PLC_ORI_F270:          - clockwise rotate 270'

enum Plc_Node_Ori
{
    PLC_NODE_ORI_B0 = 0, PLC_NODE_ORI_B90 = 1, 
	PLC_NODE_ORI_B180 = 2, PLC_NODE_ORI_B270 = 3,
    PLC_NODE_ORI_F0 = 4, PLC_NODE_ORI_F90 = 5, 
	PLC_NODE_ORI_F180 = 6, PLC_NODE_ORI_F270 = 7
};

enum Plc_Row_Ori
{
	PLC_ROW_ORI_H = 0, PLC_ROW_ORI_V = 1
};


struct Plc_Manager
{
	char				szName[80];				// Design name
	Plc_Vec_t *			vNodes;					// List of nodes
    Plc_Hash_t *        hNodes;                 // Hash of nodes
	Plc_Vec_t *			vNets;					// List of nets
	Plc_Vec_t *			vRows;					// List of rows
	// double				dX1;					// Bottom left corner
	// double				dY1;
	// double				dX2;					// Top Right corner
	// double				dY2;
};

struct Plc_Node
{
	Plc_Man_t *			pMan;					// Placement manager
	unsigned int		uId;					// Node ID
	char				szName[80];				// Node name
	Plc_Node_Type_t		eType;
	double 				dX;						// X coordinate of bottom-left corner
	double 				dY;						// Y coordinate of bottom-left corner
	double 				dW;						// Node width (X direction)
	double 				dH;						// Node height (Y direction)
	Plc_Node_Ori_t		eOri;
	Plc_Vec_t *			vPins;					// List of pins
	// double				dWeight;				// Node weight
};

struct Plc_Net
{
	Plc_Man_t *			pMan;					// Placement manager
	unsigned int		uId;					// Net ID
	char				szName[40];				// Net name
	int	            	nDeg;					// Net degree
	Plc_Vec_t *			vPins;					// List of pins
};

struct Plc_Pin
{
	Plc_Node_t *		pNode;					// Pointer to owner node
	Plc_Net_t *			pNet;					// Pointer to owner net
	Plc_Pin_Dir_t		eIO;					// IO direction
	double				dX;						// X-offset from center of owner cell
	double				dY;						// Y-offset from center of owner cell
};

struct Plc_Row
{
	Plc_Man_t *			pMan;					// Placement manager
	Plc_Row_Ori_t		eOri;					// Now only support horizontal
	double				dX;						// X coordinate of row origin
	double				dY;						// Y coordinate of row origin
	double				dH;						// Row height
	double				dW;						// Width of each site
};

struct Plc_Vec
{
	int		nCap;
	int		nSize;
	void **				pArray;

};

typedef struct Plc_Hash_Entry
{
    char szKey[80];
    void * pVal;
    struct Plc_Hash_Entry * pNext;
} Plc_Hash_Entry_t;

struct Plc_Hash
{
    int nBins;
    Plc_Hash_Entry_t ** pArray;
    int (* fHash)(const char * szKey, int nBins);
};

////////////////////////////////////////////////////////////////////////
///                     FUNCTION DEFINITIONS                         ///
////////////////////////////////////////////////////////////////////////

/*=== node.c ================================================================*/
Plc_Node_t * Plc_NodeAlloc( const char * szName );
void Plc_NodeFree();

/*=== net.c =================================================================*/
Plc_Net_t * Plc_NetAlloc( const char * szName );
void Plc_NetFree();

/*=== pin.c =================================================================*/
Plc_Pin_t * Plc_PinAlloc();
void Plc_PinFree();

/*=== pin.c =================================================================*/
Plc_Row_t * Plc_RowAlloc();
void Plc_RowFree();

/*=== man.c =================================================================*/
Plc_Man_t * Plc_ManAlloc( const char * szDesignName );
void Plc_ManFree();
Plc_Node_t * Plc_ManFindNodeName( const Plc_Man_t * pMan, const char * szName );

/*=== parser.c ==============================================================*/
Plc_Man_t * Plc_PlcFromBookshelf( const char *szFilename );
void Plc_PlcToBookshelf( const char *szFilename );

#define Plc_VecTypeEntry(pVec, type, i)                                       \
		((type *)Plc_VecEntry(pVec, i))
#define Plc_VecTypeHead(pVec, type)                                           \
		((type *)Plc_VecHead(pVec))
#define Plc_VecIterate( pVec, type, pEntry, i )                               \
    for ((i) = 0; ((i)<(pVec)->nSize) && (((pEntry)=Plc_VecTypeEntry(pVec,type,i)),1); (i)++)

static inline Plc_Vec_t * Plc_VecAlloc( int nCap )
{
	Plc_Vec_t * pVec;
	pVec = (Plc_Vec_t *)malloc(sizeof(Plc_Vec_t));
	if (nCap > 0 && nCap < 8)
	{
		nCap = 8;
	}
	pVec->nSize = 0;
	pVec->nCap = nCap;
	pVec->pArray = (nCap > 0) ? (void **)malloc(nCap*sizeof(void *)) : NULL;
	return pVec;
}

static inline Plc_Vec_t * Plc_VecDup( Plc_Vec_t * pVec )
{
	Plc_Vec_t * p;
	p = (Plc_Vec_t *)malloc(sizeof(Plc_Vec_t));
    p->nSize  = pVec->nSize;
    p->nCap   = pVec->nCap;
	p->pArray = (p->nCap > 0) ? (void **)malloc(p->nCap*sizeof(void *)) : NULL;
    memcpy( p->pArray, pVec->pArray, sizeof(void *) * pVec->nSize );
    return p;	
}

static inline void Plc_VecFree( Plc_Vec_t * pVec )
{
	free(pVec->pArray);
	free(pVec);
}

static inline int Plc_VecSize( Plc_Vec_t * p)
{
	return p->nSize;
}

static inline void * Plc_VecEntry( Plc_Vec_t * p, int i )
{
	assert(i >= 0 && i < p->nSize);
	return p->pArray[i];
}

static inline void * Plc_VecHead( Plc_Vec_t * p )
{
    assert(p->nSize > 0);
    return p->pArray[0];
}

static inline void Plc_VecGrow( Plc_Vec_t * p, int nCapMin )
{
    if ( p->nCap >= nCapMin )
        return;
    p->pArray = realloc(p->pArray, nCapMin*sizeof(void *));
    p->nCap   = nCapMin;
}

static inline void Plc_VecSet( Plc_Vec_t * p, int i, void * pEntry )
{
	if (i+1 > 2*p->nCap)
	{
		Plc_VecGrow(p, i+1);
	}
	else if (i+1 > p->nCap)
	{
		Plc_VecGrow(p, 2*p->nCap);
	}
	p->pArray[i] = pEntry;
}

static inline void Plc_VecPush( Plc_Vec_t * p, void * pEntry )
{
    if ( p->nSize == p->nCap )
    {
        if ( p->nCap < 16 )
            Plc_VecGrow( p, 16 );
        else
            Plc_VecGrow( p, 2 * p->nCap );
    }
    p->pArray[p->nSize++] = pEntry;
}

static inline void Plc_VecRemove( Plc_Vec_t * p, void * pEntry )
{
    int i;
    for (i = 0; i < p->nSize; ++i)
    {
        if (p->pArray[i] == pEntry)
        {
            break;
        }
    }
    assert(i < p->nSize);
    for (++i; i < p->nSize; ++i)
    {
        p->pArray[i-1] = p->pArray[i];
    }

    (p->nSize)--;
}

static inline Plc_Hash_t * Plc_HashAlloc( int nBins, int (* fHash)(const char * szKey, int nBins) )
{
    Plc_Hash_t * pHash;
    assert(nBins > 0);
    pHash = (Plc_Hash_t *)malloc(sizeof(Plc_Hash_t));
    pHash->nBins = nBins;
    pHash->pArray = (Plc_Hash_Entry_t **)malloc(nBins*(sizeof(Plc_Hash_Entry_t *)));
    memset(pHash->pArray, 0, nBins*sizeof(Plc_Hash_Entry_t *));
    pHash->fHash = fHash;
    return pHash;
}

static inline void Plc_HashFree( Plc_Hash_t * pHash)
{
    int i;
    assert(pHash != NULL);
    for (i = 0; i < pHash->nBins; ++i)
    {
        Plc_Hash_Entry_t * pEntry;
        for (pEntry = pHash->pArray[i]; pEntry != NULL; )
        {
            Plc_Hash_Entry_t * pNext = pEntry->pNext;
            free(pEntry);
            pEntry = pNext;
        }
    }

    free(pHash->pArray);
    free(pHash);
}

static inline void Plc_HashInsert( Plc_Hash_t * pHash, const char * szKey, void * pVal )
{
    unsigned int iKey;
    Plc_Hash_Entry_t * pEntry;

    assert(pHash != NULL);
    iKey = pHash->fHash(szKey, pHash->nBins);
    pEntry = (Plc_Hash_Entry_t *)malloc(sizeof(Plc_Hash_Entry_t));
    strncpy(pEntry->szKey, szKey, 79);
    pEntry->szKey[79] = '\0';
    pEntry->pVal = pVal;
    pEntry->pNext = pHash->pArray[iKey];
    pHash->pArray[iKey] = pEntry;
}

static inline void * Plc_HashRemove( Plc_Hash_t * pHash, const char * szKey )
{
    unsigned int iKey;
    void * pVal;
    Plc_Hash_Entry_t * pEntry;
    Plc_Hash_Entry_t ** pLast;

    assert(pHash != NULL);

    iKey = pHash->fHash(szKey, pHash->nBins);
    for (pEntry = pHash->pArray[iKey], pLast = &(pHash->pArray[iKey]); 
        pEntry != NULL; pLast = &(pEntry->pNext), pEntry = pEntry->pNext)
    {
        if (strncmp(pEntry->szKey, szKey, 80) == 0)
        {
            *pLast = pEntry->pNext;
            pVal = pEntry->pVal;
            free(pEntry);
            return pVal;
        }
    }

    return NULL;
}

static inline void * Plc_HashSearch( Plc_Hash_t * pHash, const char * szKey )
{
    int iKey;
    Plc_Hash_Entry_t * pEntry;

    assert(pHash != NULL);

    iKey = pHash->fHash(szKey, pHash->nBins);
    for (pEntry = pHash->pArray[iKey]; pEntry != NULL; pEntry = pEntry->pNext)
    {
        if (strncmp(pEntry->szKey, szKey, 80) == 0)
        {
            return pEntry->pVal;
        }
    }

    return NULL;
}

#endif
