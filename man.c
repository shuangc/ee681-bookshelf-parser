#include "placement.h"
#include <stdlib.h>
#include <string.h>

static int Plc_ManNodeHash( const char * szKey, int nBins )
{
    int i;
    int lName;
    unsigned int iKey = 0;

    assert(szKey != NULL);
    assert(nBins > 0);

    lName = strlen(szKey);
   
    for (i = 0; i < lName; ++i)
    {
        iKey = (iKey * (nBins/3) + szKey[i]) % nBins;
    }

    return iKey;
}

Plc_Man_t * Plc_ManAlloc( const char * szName )
{
	Plc_Man_t * pMan;
	pMan = (Plc_Man_t *)malloc(sizeof(Plc_Man_t));
	strncpy(pMan->szName, szName, 79);
	pMan->szName[79] = '\0';
	pMan->vNodes = Plc_VecAlloc(0);
    pMan->hNodes = Plc_HashAlloc(65536, Plc_ManNodeHash);
	pMan->vNets = Plc_VecAlloc(0);
	pMan->vRows = Plc_VecAlloc(0);
	return pMan;
}

void Plc_ManFree( Plc_Man_t * pMan)
{
    Plc_Node_t * pNode;
    Plc_Net_t * pNet;
    Plc_Row_t * pRow;
    unsigned int i;
    Plc_VecIterate(pMan->vNodes, Plc_Node_t, pNode, i)
    {
        Plc_NodeFree(pNode);
    }
	Plc_VecFree(pMan->vNodes);
    Plc_HashFree(pMan->hNodes);
    Plc_VecIterate(pMan->vNets, Plc_Net_t, pNet, i)
    {
        Plc_NetFree(pNet);
    }
	Plc_VecFree(pMan->vNets);
    Plc_VecIterate(pMan->vRows, Plc_Row_t, pRow, i)
    {
        Plc_RowFree(pRow);
    }
	Plc_VecFree(pMan->vRows);
	free(pMan);
}

Plc_Node_t * Plc_ManFindNodeName( const Plc_Man_t * pMan, const char * szName )
{
    Plc_Node_t * pNode;
    pNode = Plc_HashSearch(pMan->hNodes, szName);
    return pNode;
}

