#include "placement.h"
#include <stdlib.h>
#include <string.h>

Plc_Row_t * Plc_RowAlloc()
{
	Plc_Row_t * pRow;
	pRow = (Plc_Row_t *)malloc(sizeof(Plc_Row_t));
	return pRow;
}

void Plc_RowFree( Plc_Row_t * pRow)
{
	free(pRow);
}
